import { MydictionaryPage } from './app.po';

describe('mydictionary App', function() {
  let page: MydictionaryPage;

  beforeEach(() => {
    page = new MydictionaryPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
