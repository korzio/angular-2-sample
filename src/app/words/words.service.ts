import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

export class Word {
  public id: number;
  public text: string;
  public translation: string;
}

@Injectable()
export class WordsService {
  constructor (private http: Http) {}

  add(word) {
    const newWord = {
      text: word,
    };

    return this.http
      .get('/assets/mocks/word.json'/*, newWord, {}*/)
      .map(response => response.json());

    // https://translate.yandex.net/api/v1.5/tr.json/translate?text=test&key=test
  }

  getRecentlyAdded() {
    return this.http
      .get('/assets/mocks/words.json')
      .map(response => response.json());
  }
}
