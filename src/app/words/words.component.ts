import { Component, OnInit } from '@angular/core';
import { Word, WordsService } from './words.service'

@Component({
  selector: 'words',
  templateUrl: './words.component.html',
  styleUrls: ['./words.component.css']
})
export class WordsComponent implements OnInit {
  public words: Array<Word>;

  constructor(private wordsService: WordsService) {}

  ngOnInit() {
    this.wordsService.getRecentlyAdded()
      .subscribe(
        words => this.words = words,
        err => console.log(err)
      );
  }

  onAdded(word) {
    this.words.push(word);
  }
}
