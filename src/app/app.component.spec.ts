/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { WordComponent } from './words/word.component';
import { WordsComponent } from './words/words.component';
import { WordsService } from './words/words.service'
import { LessonsComponent } from './lessons/lessons.component';
import { SettingsComponent } from './settings/settings.component';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        WordComponent,
        WordsComponent,
        LessonsComponent,
        SettingsComponent
      ],
      providers: [WordsService],
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render words', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('words')).toBeTruthy();
  }));
});
