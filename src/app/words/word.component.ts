import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WordsService } from './words.service';

@Component({
  selector: 'word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.css']
})
export class WordComponent {
  @Output() onAdded = new EventEmitter<boolean>();
  public word: String = '';

  constructor(private wordsService: WordsService) {}

  addNewWord() {
    this.word = '';

    this.wordsService.add(this.word)
      .subscribe(
          ([newWord]) => { this.onAdded.emit(newWord); },
          err => console.log(err)
        );
  }
}
