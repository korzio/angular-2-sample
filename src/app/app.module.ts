import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { WordComponent } from './words/word.component';
import { WordsComponent } from './words/words.component';
import { WordsService } from './words/words.service'
import { LessonsComponent } from './lessons/lessons.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    WordComponent,
    WordsComponent,
    LessonsComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [WordsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
